﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSPAlgorithms.DataStructures
{
    public class Signal
    {
        public List<float> Samples { get; set; }
        public List<int> SamplesIndices { get; set; }
        public List<float> Frequencies { get; set; }
        public List<float> FrequenciesAmplitudes { get; set; }
        public List<float> FrequenciesPhaseShifts { get; set; }
        public bool Periodic { get; set; }
        public bool Folding = false;
        public Signal(List<float> _SignalSamples, bool _periodic)
        {
            Samples = _SignalSamples;
            Periodic = _periodic;
            SamplesIndices = new List<int>(_SignalSamples.Count);

            for (int i = 0; i < _SignalSamples.Count; i++)
                SamplesIndices.Add(i);
        }
        public Signal(List<float> _SignalSamples, List<int> _SignalIndixes, bool _periodic)
        {
            Samples = _SignalSamples;
            Periodic = _periodic;
            SamplesIndices = _SignalIndixes;
        }
        public Signal(List<float> _SignalSamples, List<int> _SignalIndixes, bool _Folding, bool _periodic)
        {
            Samples = _SignalSamples;
            Folding = _Folding;
            SamplesIndices = _SignalIndixes;
            Periodic = _periodic;
        }
        public Signal(bool _periodic, List<float> _SignalFrquencies, List<float> _SignalFrequenciesAmplitudes, List<float> _SignalFrequenciesPhaseShifts)
        {
            Periodic = _periodic;
            Frequencies = _SignalFrquencies;
            FrequenciesAmplitudes = _SignalFrequenciesAmplitudes;
            FrequenciesPhaseShifts = _SignalFrequenciesPhaseShifts;
        }
        public Signal(List<float> _SignalSamples, bool _periodic, List<float> _SignalFrequencies, List<float> _SignalFrequenciesAmplitudes, List<float> _SignalFrequenciesPhaseShifts)
        {
            Periodic = _periodic;
            Samples = _SignalSamples;
            SamplesIndices = new List<int>(_SignalSamples.Count);

            for (int i = 0; i < _SignalSamples.Count; i++)
                SamplesIndices.Add(i);

            Frequencies = _SignalFrequencies;
            FrequenciesAmplitudes = _SignalFrequenciesAmplitudes;
            FrequenciesPhaseShifts = _SignalFrequenciesPhaseShifts;
        }
        public Signal(List<float> _SignalSamples, List<int> _SignalIndixes, bool _periodic, List<float> _SignalFrequencies, List<float> _SignalFrequenciesAmplitudes, List<float> _SignalFrequenciesPhaseShifts)
        {
            Samples = _SignalSamples;
            Periodic = _periodic;
            SamplesIndices = _SignalIndixes;
            Frequencies = _SignalFrequencies;
            FrequenciesAmplitudes = _SignalFrequenciesAmplitudes;
            FrequenciesPhaseShifts = _SignalFrequenciesPhaseShifts;
        }

        public static Signal operator +(Signal sig1, Signal sig2)
        {
            List<float> samples = new List<float>();
            int n = Math.Max(sig1.Samples.Count, sig2.Samples.Count);
            for(int i = 0; i < n; i++)
            {
                if(i < sig1.Samples.Count && i < sig2.Samples.Count)
                    samples.Add(sig1.Samples[i] + sig2.Samples[i]);
                else if(i < sig1.Samples.Count)
                    samples.Add(sig1.Samples[i]);
                else if (i < sig2.Samples.Count)
                    samples.Add(sig2.Samples[i]);
            }
            return new Signal(samples, false);
        }
        public static Signal operator *(Signal sig, float constant)
        {
            List<float> samples = new List<float>();

            foreach (float i in sig.Samples)
            {
                samples.Add(i * constant);
            }
            return new Signal(samples, false);
        }

        public Signal Normalize(float minRange, float maxRange)
        {
            List<float> samples = new List<float>();
            foreach (float i in Samples)
            {
                samples.Add((i - Samples.Min()) /
                    (Samples.Max() - Samples.Min()) *
                    (maxRange - minRange) + minRange);
            }

            return new Signal(samples, false);
        }
    }
}
