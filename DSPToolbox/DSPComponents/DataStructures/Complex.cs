﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSPAlgorithms.DataStructures
{
    public class Complex
    {
        public double real { get; set; }
        public double imaginery { get; set; }
        public Complex()
        {
            real = 0;
            imaginery = 0;
        }
        public Complex(double r, double i)
        {
            real = r;
            imaginery = i;
        }
        public static Complex operator +(Complex com1, Complex com2)
        {   
            return new Complex(com1.real + com2.real, com1.imaginery + com2.imaginery);
        }
        public static Complex operator -(Complex com1, Complex com2)
        {
            return new Complex(com1.real - com2.real, com1.imaginery - com2.imaginery);
        }
        public static Complex operator *(Complex com1, Complex com2)
        {            
            return new Complex(com1.real * com2.real - com1.imaginery * com2.imaginery,
                               com1.imaginery * com2.real + com1.real * com2.imaginery);
        }
        public double Magnitude()
        {
            return Math.Sqrt(real * real + imaginery * imaginery);
        }
        public double PhaseShift()
        {
            return Math.Atan2(imaginery, real);
        }
    }
}
