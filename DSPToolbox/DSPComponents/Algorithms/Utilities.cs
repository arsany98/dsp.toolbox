﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using DSPAlgorithms.DataStructures;

namespace DSPAlgorithms.Algorithms
{
    public class AlgorithmsUtilities
    {
        // Implement here any functionalities you need to share between Algorithm classes

        public static List<Complex> DFT(List<Complex> input, int N, bool inverse)
        {
            List<Complex> fourier = new List<Complex>();
            for (int k = 0; k < N; k++)
            {
                Complex f = new Complex();
                for (int i = 0; i < N; i++)
                {
                    Complex e;
                    if (inverse)
                        e = new Complex(Math.Cos(k * 2 * Math.PI * i / N),
                                        Math.Sin(k * 2 * Math.PI * i / N));
                    else
                        e = new Complex(Math.Cos(k * 2 * Math.PI * i / N),
                                       -Math.Sin(k * 2 * Math.PI * i / N));
                    f += input[i] * e;
                }
                fourier.Add(f);
            }
            return fourier;
        }
        public static List<Complex> FFT(List<Complex> input, int N, bool inverse)
        {
            if (N == 2)
                return new List<Complex> { input[0] + input[1],
                                           input[0] - input[1] };                
            

            List<Complex> evenSamples = new List<Complex>();
            List<Complex> oddSamples = new List<Complex>();
            for(int i = 0; i < N; i++)
            {
                if (i % 2 == 0)
                    evenSamples.Add(input[i]);
                else
                    oddSamples.Add(input[i]);
            }
            List<Complex> fft_1 = FFT(evenSamples, N / 2, inverse);
            List<Complex> fft_2 = FFT(oddSamples, N / 2, inverse);

            List<Complex> returnList = new List<Complex>();
            for (int i = 0; i < N; i++)
                returnList.Add(new Complex());
            for (int k = 0; k < N / 2; k++)
            {
                Complex W;
                if(inverse)
                    W = new Complex(Math.Cos(2 * Math.PI * k / N),
                                    Math.Sin(2 * Math.PI * k / N));
                else
                    W = new Complex(Math.Cos(2 * Math.PI * k / N),
                                    -Math.Sin(2 * Math.PI * k / N));
                returnList[k] = fft_1[k] + W * fft_2[k];
                returnList[k + N / 2] = fft_1[k] - W * fft_2[k];

            }
            return returnList;
        }
        public static void SaveSignalFrequencyDomain(Signal sig, string filePath)
        {
            StreamWriter streamSaver = new StreamWriter(filePath);

            streamSaver.WriteLine(1);
            streamSaver.WriteLine(0);
            streamSaver.WriteLine(sig.Frequencies.Count);

            for (int i = 0; i < sig.Frequencies.Count; i++)
            {
                streamSaver.Write(sig.Frequencies[i]);
                streamSaver.Write(" " + sig.FrequenciesAmplitudes[i]);
                streamSaver.WriteLine(" " + sig.FrequenciesPhaseShifts[i]);
            }

            streamSaver.Flush();
            streamSaver.Close();
        }

    }
}
