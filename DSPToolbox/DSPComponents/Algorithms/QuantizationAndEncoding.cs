﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSPAlgorithms.DataStructures;

namespace DSPAlgorithms.Algorithms
{
    public class QuantizationAndEncoding : Algorithm
    {
        // You will have only one of (InputLevel or InputNumBits), the other property will take a negative value
        // If InputNumBits is given, you need to calculate and set InputLevel value and vice versa
        public int InputLevel { get; set; }
        public int InputNumBits { get; set; }
        public Signal InputSignal { get; set; }
        public Signal OutputQuantizedSignal { get; set; }
        public List<int> OutputIntervalIndices { get; set; }
        public List<string> OutputEncodedSignal { get; set; }
        public List<float> OutputSamplesError { get; set; }

        public override void Run()
        {          
            if (InputNumBits > 0)
                InputLevel = (int)Math.Pow(2, InputNumBits);
            else if (InputLevel > 0)
                InputNumBits = (int)Math.Ceiling(Math.Log(InputLevel, 2));

            float delta = (InputSignal.Samples.Max() - InputSignal.Samples.Min()) / InputLevel;
            List<float> levels = new List<float>();
            float signalMin = InputSignal.Samples.Min();
            for (int i = 0; i < InputLevel; i++)
            {
                levels.Add(signalMin);
                signalMin += delta;
            }
            OutputIntervalIndices = new List<int>();
            List<float> quantized = new List<float>();
            OutputEncodedSignal = new List<string>();
            OutputSamplesError = new List<float>();
            for(int i = 0; i < InputSignal.Samples.Count; i++)
            {
                int intervalIndex = levels.Where(x => x <= InputSignal.Samples[i]).Count();
                OutputIntervalIndices.Add(intervalIndex);
                float rangeMin = levels[intervalIndex - 1];
                float rangeMax = rangeMin + delta;
                float midPoint = (rangeMax + rangeMin) / 2;
                quantized.Add(midPoint);
                OutputEncodedSignal.Add(Convert.ToString(intervalIndex - 1, 2).PadLeft(InputNumBits,'0'));
                OutputSamplesError.Add(quantized[i] - InputSignal.Samples[i]);
            }
            OutputQuantizedSignal = new Signal(quantized, false);
        }
    }
}
