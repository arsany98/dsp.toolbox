﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSPAlgorithms.DataStructures;
using DSPAlgorithms.Algorithms.Filters;
using DSPAlgorithms.Algorithms.Windows;

namespace DSPAlgorithms.Algorithms
{
    public class FIR : Algorithm
    {
        public Signal InputTimeDomainSignal { get; set; }
        public FILTER_TYPES InputFilterType { get; set; }

        public float InputFS { get; set; } //sampling frequency
        public float InputCutOffFrequency { get; set; } //passband edge frequency
        public float InputF1 { get; set; }
        public float InputF2 { get; set; }
        public float InputStopBandAttenuation { get; set; } //stopband attenuation
        public float InputTransitionBand { get; set; } //transition width
        public Signal OutputHn { get; set; }
        public Signal OutputYn { get; set; }

        public override void Run()
        {
            OutputHn = new Signal(new List<float>(), true);
            OutputYn = new Signal(new List<float>(), true);

            DirectConvolution dc = new DirectConvolution();

            WINDOW_TYPES WindowType = DetectWindowType(InputStopBandAttenuation);
            if (WindowType == WINDOW_TYPES.HAMMING)
            {
                Hamming Window = new Hamming(InputTransitionBand, InputFS);

                if (InputFilterType == FILTER_TYPES.LOW)
                {
                    float Fc_ = (InputCutOffFrequency + (InputTransitionBand / 2)) / InputFS;
                    Lowpass lp = new Lowpass(Fc_, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(lp.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }

                    List<float> l = new List<float> (OutputHn.Samples);
                    List<int> ll = new List<int>(OutputHn.SamplesIndices);

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = new Signal(l,ll, true);

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }
                else if (InputFilterType == FILTER_TYPES.HIGH)
                {
                    float Fc_ = (InputCutOffFrequency - (InputTransitionBand / 2)) / InputFS;
                    Highpass hp = new Highpass(Fc_, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(hp.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }

                    List<float> l = new List<float>(OutputHn.Samples);
                    List<int> ll = new List<int>(OutputHn.SamplesIndices);

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = new Signal(l, ll, true);

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }
                else if (InputFilterType == FILTER_TYPES.BAND_PASS)
                {
                    float F1 = (InputF1 - (InputTransitionBand / 2)) / InputFS;
                    float F2 = (InputF2 + (InputTransitionBand / 2)) / InputFS;

                    Bandpass Bp = new Bandpass(F1, F2, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(Bp.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }

                    List<float> l = new List<float>(OutputHn.Samples);
                    List<int> ll = new List<int>(OutputHn.SamplesIndices);

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = new Signal(l, ll, true);

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }
                else
                {
                    float F1 = (InputF1 + (InputTransitionBand / 2)) / InputFS;
                    float F2 = (InputF2 - (InputTransitionBand / 2)) / InputFS;

                    Bandstop Bs = new Bandstop(F1, F2, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(Bs.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }

                    List<float> l = new List<float>(OutputHn.Samples);
                    List<int> ll = new List<int>(OutputHn.SamplesIndices);

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = new Signal(l, ll, true);

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }

            }
            else if (WindowType == WINDOW_TYPES.RECTANGULAR)
            {
                Rectangular Window = new Rectangular(InputTransitionBand, InputFS);

                if (InputFilterType == FILTER_TYPES.LOW)
                {
                    float Fc_ = (InputCutOffFrequency + (InputTransitionBand / 2)) / InputFS;
                    Lowpass lp = new Lowpass(Fc_, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(lp.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }

                    List<float> l = new List<float>(OutputHn.Samples);
                    List<int> ll = new List<int>(OutputHn.SamplesIndices);

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = new Signal(l, ll, true);

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }
                else if (InputFilterType == FILTER_TYPES.HIGH)
                {
                    float Fc_ = (InputCutOffFrequency - (InputTransitionBand / 2)) / InputFS;
                    Highpass hp = new Highpass(Fc_, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(hp.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }

                    List<float> l = new List<float>(OutputHn.Samples);
                    List<int> ll = new List<int>(OutputHn.SamplesIndices);

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = new Signal(l, ll, true);

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }
                else if (InputFilterType == FILTER_TYPES.BAND_PASS)
                {
                    float F1 = (InputF1 - (InputTransitionBand / 2)) / InputFS;
                    float F2 = (InputF2 + (InputTransitionBand / 2)) / InputFS;

                    Bandpass Bp = new Bandpass(F1, F2, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(Bp.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }
                    List<float> l = new List<float>(OutputHn.Samples);
                    List<int> ll = new List<int>(OutputHn.SamplesIndices);

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = new Signal(l, ll, true);

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }
                else
                {
                    float F1 = (InputF1 + (InputTransitionBand / 2)) / InputFS;
                    float F2 = (InputF2 - (InputTransitionBand / 2)) / InputFS;

                    Bandstop Bs = new Bandstop(F1, F2, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(Bs.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }

                    List<float> l = new List<float>(OutputHn.Samples);
                    List<int> ll = new List<int>(OutputHn.SamplesIndices);

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = new Signal(l, ll, true);

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }
            }
            else if (WindowType == WINDOW_TYPES.HANNING)
            {
                Hanning Window = new Hanning(InputTransitionBand, InputFS);

                if (InputFilterType == FILTER_TYPES.LOW)
                {
                    float Fc_ = (InputCutOffFrequency + (InputTransitionBand / 2)) / InputFS;
                    Lowpass lp = new Lowpass(Fc_, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(lp.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }

                    List<float> l = new List<float>(OutputHn.Samples);
                    List<int> ll = new List<int>(OutputHn.SamplesIndices);

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = new Signal(l, ll, true);

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }
                else if (InputFilterType == FILTER_TYPES.HIGH)
                {
                    float Fc_ = (InputCutOffFrequency - (InputTransitionBand / 2)) / InputFS;
                    Highpass hp = new Highpass(Fc_, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(hp.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }

                    List<float> l = new List<float>(OutputHn.Samples);
                    List<int> ll = new List<int>(OutputHn.SamplesIndices);

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = new Signal(l, ll, true);

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }
                else if (InputFilterType == FILTER_TYPES.BAND_PASS)
                {
                    float F1 = (InputF1 + (InputTransitionBand / 2)) / InputFS;
                    float F2 = (InputF2 - (InputTransitionBand / 2)) / InputFS;

                    Bandpass Bp = new Bandpass(F1, F2, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(Bp.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }

                    List<float> l = new List<float>(OutputHn.Samples);
                    List<int> ll = new List<int>(OutputHn.SamplesIndices);

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = new Signal(l, ll, true);

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }
                else
                {
                    float F1 = (InputF1 - (InputTransitionBand / 2)) / InputFS;
                    float F2 = (InputF2 + (InputTransitionBand / 2)) / InputFS;

                    Bandstop Bs = new Bandstop(F1, F2, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(Bs.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }

                    List<float> l = new List<float>(OutputHn.Samples);
                    List<int> ll = new List<int>(OutputHn.SamplesIndices);

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = new Signal(l, ll, true);

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }
            }
            else
            {
                Blackman Window = new Blackman(InputTransitionBand, InputFS);

                if (InputFilterType == FILTER_TYPES.LOW)
                {
                    float Fc_ = (InputCutOffFrequency + (InputTransitionBand / 2)) / InputFS;
                    Lowpass lp = new Lowpass(Fc_, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(lp.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = OutputHn;

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }
                else if (InputFilterType == FILTER_TYPES.HIGH)
                {
                    float Fc_ = (InputCutOffFrequency - (InputTransitionBand / 2)) / InputFS;
                    Highpass hp = new Highpass(Fc_, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(hp.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }

                    List<float> l = new List<float>(OutputHn.Samples);
                    List<int> ll = new List<int>(OutputHn.SamplesIndices);

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = new Signal(l, ll, true);

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }
                else if (InputFilterType == FILTER_TYPES.BAND_PASS)
                {
                    float F1 = (InputF1 - (InputTransitionBand / 2)) / InputFS;
                    float F2 = (InputF2 + (InputTransitionBand / 2)) / InputFS;

                    Bandpass Bp = new Bandpass(F1, F2, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(Bp.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }

                    List<float> l = new List<float>(OutputHn.Samples);
                    List<int> ll = new List<int>(OutputHn.SamplesIndices);

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = new Signal(l, ll, true);

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }
                else
                {
                    float F1 = (InputF1 + (InputTransitionBand / 2)) / InputFS;
                    float F2 = (InputF2 - (InputTransitionBand / 2)) / InputFS;

                    Bandstop Bs = new Bandstop(F1, F2, InputTimeDomainSignal, Window.N);
                    List<float> Hn = new List<float>();
                    for (int i = 0; i < Window.N; i++)
                    {
                        Hn.Add(Bs.Output_Hd[i] * Window.Output_Wn[i]);
                    }

                    int k = -1 * ((Window.N - 1) / 2);
                    OutputHn.Samples = Hn;

                    for (int i = k; i <= -1 * k; i++)
                    {
                        OutputHn.SamplesIndices.Add(i);
                    }
                    List<float> l = new List<float>(OutputHn.Samples);
                    List<int> ll = new List<int>(OutputHn.SamplesIndices);

                    dc.InputSignal1 = InputTimeDomainSignal;
                    dc.InputSignal2 = new Signal(l, ll, true);

                    dc.Run();

                    OutputYn = dc.OutputConvolvedSignal;
                }

            }
        }
        public WINDOW_TYPES DetectWindowType(float InputStopBandAttenuation)
        {
            if (InputStopBandAttenuation > 0 && InputStopBandAttenuation <= 21)
                return WINDOW_TYPES.RECTANGULAR;
            else if (InputStopBandAttenuation > 21 && InputStopBandAttenuation <= 44)
                return WINDOW_TYPES.HANNING;
            else if (InputStopBandAttenuation > 44 && InputStopBandAttenuation <= 53)
                return WINDOW_TYPES.HAMMING;
            else //if (InputStopBandAttenuation > 53 && InputStopBandAttenuation <= 74)
                return WINDOW_TYPES.BLACKMAN;

        }                
    }
}
