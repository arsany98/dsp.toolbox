﻿using DSPAlgorithms.DataStructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSPAlgorithms.Algorithms.Windows
{
    public class Hamming
    {
        public float PassbandRipple { get; set; }
        public float MainlopeRelativetoSidelope { get; set; }
        public float StopbandAttenuation { get; set; }
        public float WindowFunction { get; set; }

        public float TW { get; set; }
        public float SF { get; set; }
        public int N { get; set; }
        public List<float> Output_Wn { get; set; }
        public Hamming(float tw, float sf)
        {
            this.TW = tw;
            this.SF = sf;
            this.N = Convert.ToInt32(3.3/(tw/sf)) ;
            if (this.N % 2 == 0)
                this.N++;
            this.PassbandRipple = (float)0.0194;
            this.MainlopeRelativetoSidelope = 41;
            this.StopbandAttenuation = 53;

            Wn();
        }
        public void Wn()
        {
            List<float> temp = new List<float>();

            double zero = 0.54 + (0.46 * Math.Cos(2 * Math.PI * 0 / N));
            for (int n = 1; n <= (N - 1) / 2; n++)
            {
                double op = 0.54 + (0.46 * Math.Cos(2 * Math.PI * n / N));
                temp.Add((float)op);
            }
            temp.Reverse();
            Output_Wn = new List<float>(temp);
            Output_Wn.Add((float)zero);
            temp.Reverse();
            Output_Wn.AddRange(temp);
        }

    }
}
