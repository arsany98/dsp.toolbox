﻿using DSPAlgorithms.DataStructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSPAlgorithms.Algorithms.Windows
{
    public class Rectangular
    {
        public float PassbandRipple { get; set; }
        public float MainlopeRelativetoSidelope { get; set; }
        public float StopbandAttenuation { get; set; }
        public float WindowFunction { get; set; }

        public float TW { get; set; }
        public float SF { get; set; }
        public int N { get; set; }
        public List<float> Output_Wn { get; set; }
        public Rectangular(float tw, float sf)
        {
            this.TW = tw;
            this.SF = sf;
            this.N = Convert.ToInt32(0.9 / (tw / sf));
            if (this.N % 2 == 0)
                this.N++;
            this.PassbandRipple = (float)0.7416;
            this.MainlopeRelativetoSidelope = 13;
            this.StopbandAttenuation = 21;

            Wn();
        }
        public void Wn()
        {
            List<float> temp = new List<float>();

            double zero = 1;
            for (int n = 1; n <= (N - 1) / 2; n++)
            {
                double op = 1;
                temp.Add((float)op);
            }
            temp.Reverse();
            Output_Wn = new List<float>(temp);
            Output_Wn.Add((float)zero);
            temp.Reverse();
            Output_Wn.AddRange(temp);
        }

    }
}
