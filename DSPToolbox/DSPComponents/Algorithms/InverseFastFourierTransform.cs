﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSPAlgorithms.DataStructures;

namespace DSPAlgorithms.Algorithms
{
    public class InverseFastFourierTransform : Algorithm
    {
        public Signal InputFreqDomainSignal { get; set; }
        public Signal OutputTimeDomainSignal { get; set; }

        public override void Run()
        {
            int N = InputFreqDomainSignal.Frequencies.Count;
            List<Complex> input = new List<Complex>();
            for (int i = 0; i < N; i++)
            {
                input.Add(new Complex(InputFreqDomainSignal.FrequenciesAmplitudes[i] * Math.Cos(InputFreqDomainSignal.FrequenciesPhaseShifts[i]),
                                      InputFreqDomainSignal.FrequenciesAmplitudes[i] * Math.Sin(InputFreqDomainSignal.FrequenciesPhaseShifts[i])));
            }
            var fourier = AlgorithmsUtilities.FFT(input, N, true);
            List<float> samples = new List<float>();
            foreach (var f in fourier)
            {
                samples.Add((float)Math.Round(f.real / N, 3));
            }
            OutputTimeDomainSignal = new Signal(samples, false);
        }
    }
}
