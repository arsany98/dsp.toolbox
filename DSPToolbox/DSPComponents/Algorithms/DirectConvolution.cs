﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSPAlgorithms.DataStructures;

namespace DSPAlgorithms.Algorithms
{
    public class DirectConvolution : Algorithm
    {
        public Signal InputSignal1 { get; set; }
        public Signal InputSignal2 { get; set; }
        public Signal OutputConvolvedSignal { get; set; }

        /// <summary>
        /// Convolved InputSignal1 (considered as X) with InputSignal2 (considered as H)
        /// </summary>
        public override void Run()
        {
            int start = InputSignal1.SamplesIndices[0] + InputSignal2.SamplesIndices[0];

            Signal x = new Signal(new List<float>() { }, new List<int>() { }, false);
            int indx = 0;
            int m = InputSignal1.SamplesIndices[0] - InputSignal2.Samples.Count + 1;

            for(int i = 0; i < InputSignal1.Samples.Count + 2*(InputSignal2.Samples.Count-1); i++)
            {
                if (i < InputSignal2.Samples.Count-1)
                {
                    x.Samples.Add(0);
                    x.SamplesIndices.Add(m);
                    m++;
                }
                else if(i >= InputSignal2.Samples.Count-1 + InputSignal1.Samples.Count)
                {
                    x.Samples.Add(0);
                    x.SamplesIndices.Add(x.SamplesIndices.Last()+1);
                }
                else
                {
                    x.Samples.Add(InputSignal1.Samples[indx]);
                    x.SamplesIndices.Add(InputSignal1.SamplesIndices[indx]);
                    indx++;
                }
            }


            Folder f = new Folder();
            f.InputSignal = new Signal(InputSignal2.Samples,InputSignal2.SamplesIndices,false);
            f.Run();
            var h = f.OutputFoldedSignal;

            int temp = InputSignal2.Samples.Count;
            while (h.Samples.Count != x.Samples.Count)
            {
                h.Samples.Add(0);
                h.SamplesIndices.Add(h.SamplesIndices.Last() + 1);
            }

            List<float> c = new List<float>();

            Shifter s = new Shifter();

            for (int k = 0; k < InputSignal1.Samples.Count + temp -1; k++)
            { 
                float sum = 0;
                for (int j = 0; j < x.Samples.Count; j++)
                {
                    sum += x.Samples[j] * h.Samples[j];
                }
                c.Add(sum);
                s.InputSignal = h;
                s.ShiftingValue = -1;
                s.Run();


                float t = s.OutputShiftedSignal.Samples[s.OutputShiftedSignal.Samples.Count - 1];
                for (int i = s.OutputShiftedSignal.Samples.Count - 1 ; i > 0; i--)
                    s.OutputShiftedSignal.Samples[i] = s.OutputShiftedSignal.Samples[i - 1];
                s.OutputShiftedSignal.Samples[0] = t;
                h = s.OutputShiftedSignal;
            }
            for(int i = c.Count - 1; i >=0;i-- )
            {
                if (c[i] != 0)
                    break;
                c.Remove(0);
            }

            List<int> indices = new List<int>();
            for(int i = 0; i < c.Count; i++)
            {
                indices.Add(start);
                start++;
            }

            OutputConvolvedSignal = new Signal(c, indices, false);

        }
    }
}
