﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSPAlgorithms.DataStructures;

namespace DSPAlgorithms.Algorithms
{
    public class DirectCorrelation : Algorithm
    {
        public Signal InputSignal1 { get; set; }
        public Signal InputSignal2 { get; set; }
        public List<float> OutputNonNormalizedCorrelation { get; set; }
        public List<float> OutputNormalizedCorrelation { get; set; }

        public override void Run()
        {
            bool periodic = false;
            List<float> x1 = new List<float>(InputSignal1.Samples);
            List<float> x2;
            if (InputSignal2==null)
            {
                x2 = new List<float>(InputSignal1.Samples);
                periodic = InputSignal1.Periodic;
            }
            else
            {
                x2 = new List<float>(InputSignal2.Samples);
                periodic = InputSignal2.Periodic;
            }
            List<float> r12 = new List<float>();
            int N;
            if (x1.Count == x2.Count)
                N = x1.Count;
            else
            {
                N = x1.Count + x2.Count - 1;
                for (int i = x1.Count; i < N; i++)
                    x1.Add(0);
                for (int i = x2.Count; i < N; i++)
                    x2.Add(0);
            }
            for(int i = 0; i < N; i++)
            {
                float sum = 0;
                for (int n = 0; n < N; n++)
                {
                    if (periodic)
                        sum += x1[n] * x2[(n + i) % N];
                    else if(n + i < N)
                        sum += x1[n] * x2[n + i];
                }
                sum /= N;
                r12.Add(sum);
            }
            List<float> r12Norm = new List<float>();
            float x1Sum = 0;
            float x2Sum = 0;
            for (int i = 0; i < N; i++)
            {
                x1Sum += x1[i] * x1[i];
                x2Sum += x2[i] * x2[i];
            }
            float denominator = 1f/N * (float)Math.Sqrt(x1Sum * x2Sum);
            for (int n = 0; n < N; n++)
            {
                r12Norm.Add(r12[n] / denominator);
            }
            OutputNonNormalizedCorrelation = r12;
            OutputNormalizedCorrelation = r12Norm;
        }
    }
}