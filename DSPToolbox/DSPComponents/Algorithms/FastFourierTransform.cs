﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSPAlgorithms.DataStructures;

namespace DSPAlgorithms.Algorithms
{
    public class FastFourierTransform : Algorithm
    {
        public Signal InputTimeDomainSignal { get; set; }
        public int InputSamplingFrequency { get; set; }
        public Signal OutputFreqDomainSignal { get; set; }

        public override void Run()
        {
            int N = InputTimeDomainSignal.Samples.Count;
            List<Complex> input = new List<Complex>();
            foreach(var s in InputTimeDomainSignal.Samples)
            {
                input.Add(new Complex(s, 0));
            }
            var fourier = AlgorithmsUtilities.FFT(input, N, false);
            List<float> frequencies = new List<float>();
            List<float> amplitudes = new List<float>();
            List<float> phaseShifts = new List<float>();
            for (int k = 0; k < fourier.Count; k++)
            {
                amplitudes.Add((float)fourier[k].Magnitude());
                phaseShifts.Add((float)fourier[k].PhaseShift());
                frequencies.Add(k * InputSamplingFrequency / N);
            }
            OutputFreqDomainSignal = new Signal(false, frequencies, amplitudes, phaseShifts);
        }
    }
}
