﻿using DSPAlgorithms.DataStructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSPAlgorithms.Algorithms
{
    public class RemoveDCComponent : Algorithm
    {
        public Signal InputSignal { get; set; }
        public Signal OutputSignal { get; set; }

        public override void Run()
        {
            List<float> samples = new List<float>();
            int N = InputSignal.Samples.Count;
            List<Complex> input = new List<Complex>();
            foreach (var s in InputSignal.Samples)
            {
                input.Add(new Complex(s, 0));
            }
            Complex fourier = AlgorithmsUtilities.DFT(input, N, false)[0];

            double DC = fourier.real / N;
            foreach (var sample in InputSignal.Samples)
            {
                samples.Add((float)Math.Round(sample - DC, 3));
            }
            OutputSignal = new Signal(samples, false);
        }
    }
}
