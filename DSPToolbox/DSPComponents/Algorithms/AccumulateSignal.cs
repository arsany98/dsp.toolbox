﻿using DSPAlgorithms.DataStructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSPAlgorithms.Algorithms
{
    public class AccumulateSignal : Algorithm
    {
        public Signal InputSignal { get; set; }
        public float OutputAccumlate { get; set; }

        public int N { get; set; }

        public override void Run()
        {
            float sum = 0;
            for (int i = 0; i <= N; i++)
                sum+=InputSignal.Samples[i];

            OutputAccumlate = sum;
        }
    }
}
