﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSPAlgorithms.DataStructures;

namespace DSPAlgorithms.Algorithms.Filters
{
    public class Bandpass
    {
        public float F1 { get; set; }
        public float F2 { get; set; }
        public int N { get; set; }
        public Signal InputSignal { get; set; }
        public List<float> Output_Hd { get; set; }
        public Bandpass(float f1, float f2, Signal inputsignal, int n)
        {
            this.F1 = f1;
            this.F2 = f2;

            this.InputSignal = inputsignal;
            this.N = n;

            Hd();
        }

        public void Hd()
        {
            double zero = 2*(F2 - F1);
            List<float> temp = new List<float>();
            for (int n = 1; n <= ((N - 1) / 2); n++)
            {
                double op = ((2 * F2) * ((Math.Sin(n * 2 * Math.PI * F2) / (n * 2 * Math.PI * F2)))) - ((2 * F1) * ((Math.Sin(n * 2 * Math.PI * F1) / (n * 2 * Math.PI * F1))));
                temp.Add((float)op);
            }
            temp.Reverse();
            Output_Hd = new List<float>(temp);
            Output_Hd.Add((float)zero);
            temp.Reverse();
            Output_Hd.AddRange(temp);
        }

    }
}
