﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSPAlgorithms.DataStructures;

namespace DSPAlgorithms.Algorithms.Filters
{
    public class Highpass
    {
        public float Fc { get; set; }
        public int N { get; set; }
        public Signal InputSignal { get; set; }
        public List<float> Output_Hd { get; set; }
        public Highpass(float Fc, Signal InputSignal, int N)
        {
            this.Fc = Fc;
            this.InputSignal = InputSignal;
            this.N = N;

            Hd();
        }

        public void Hd()
        {
            double zero = 1-(2 * Fc);
            List<float> temp = new List<float>();
            for (int n = 1; n <= ((N - 1) / 2); n++)
            {
                double op = (-2 * Fc) * ((Math.Sin(n * 2 * Math.PI * Fc) / (n * 2 * Math.PI * Fc)));
                temp.Add((float)op);
            }
            temp.Reverse();
            Output_Hd = new List<float>(temp);
            Output_Hd.Add((float)zero);
            temp.Reverse();
            Output_Hd.AddRange(temp);
        }

    }
}
