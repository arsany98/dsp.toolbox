﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSPAlgorithms.DataStructures;

namespace DSPAlgorithms.Algorithms
{
    public class FastCorrelation : Algorithm
    {
        public Signal InputSignal1 { get; set; }
        public Signal InputSignal2 { get; set; }
        public List<float> OutputNonNormalizedCorrelation { get; set; }
        public List<float> OutputNormalizedCorrelation { get; set; }

        public override void Run()
        {
            bool periodic = false;
            List<Complex> x1 = new List<Complex>();
            foreach (var s in InputSignal1.Samples)
            {
                x1.Add(new Complex(s, 0));
            }
            List<Complex> x2;
            if (InputSignal2 == null)
            {
                x2 = new List<Complex>();
                foreach (var s in InputSignal1.Samples)
                {
                    x2.Add(new Complex(s, 0));
                }
                periodic = InputSignal1.Periodic;
            }
            else
            {
                x2 = new List<Complex>();
                foreach (var s in InputSignal2.Samples)
                {
                    x2.Add(new Complex(s, 0));
                }
                periodic = InputSignal2.Periodic;
            }
            List<float> r12 = new List<float>();
            int N;
            if (x1.Count == x2.Count)
                N = x1.Count;
            else
            {
                N = x1.Count + x2.Count - 1;
                for (int i = x1.Count; i < N; i++)
                    x1.Add(new Complex(0, 0));
                for (int i = x2.Count; i < N; i++)
                    x2.Add(new Complex(0, 0));
            }
                        
            List<Complex> x1f = AlgorithmsUtilities.FFT(x1, N, false);
            List<Complex> x2f = AlgorithmsUtilities.FFT(x2, N, false);
            foreach(Complex c in x1f)
            {
                c.imaginery = -c.imaginery;
            }
            List<Complex> x1x2 = new List<Complex>();
            for (int i = 0; i < N; i++)
            {
                x1x2.Add(x1f[i] * x2f[i]);
            }
            List<Complex> ifft = AlgorithmsUtilities.FFT(x1x2, N, true);
            for ( int i = 0; i < N; i++)
            {
                r12.Add(1f / N * (float)ifft[i].real/N);
            }
            List<float> r12Norm = new List<float>();
            float x1Sum = 0;
            float x2Sum = 0;
            for (int i = 0; i < N; i++)
            {
                x1Sum += (float)(x1[i].real * x1[i].real);
                x2Sum += (float)(x2[i].real * x2[i].real);
            }
            float denominator = 1f / N * (float)Math.Sqrt(x1Sum * x2Sum);
            for (int n = 0; n < N; n++)
            {
                r12Norm.Add(r12[n] / denominator);
            }
            OutputNonNormalizedCorrelation = r12;
            OutputNormalizedCorrelation = r12Norm;
        }
    }
}