﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSPAlgorithms.Algorithms;
using DSPAlgorithms.DataStructures;

namespace DSPComponentsUnitTest
{
      [TestClass]
    public class AccumulateSignalTestCases
    {
        [TestMethod]
        public void AccumulateSignalTestCases_0()
        {
            // test case 1 ..
            Signal sig1 = new Signal(new List<float>(){ 1,2,3,4,5,6,7,8,9,10 },false);
            float expectedOutput = 1;

            AccumulateSignal m = new AccumulateSignal();
            m.InputSignal = sig1;
            m.N = 0;

            m.Run();

            Assert.IsTrue(expectedOutput == m.OutputAccumlate);
        }
        [TestMethod]
        public void AccumulateSignalTestCases_4()
        {
            // test case 1 ..
            Signal sig1 = new Signal(new List<float>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, false);
            float expectedOutput = 15;

            AccumulateSignal m = new AccumulateSignal();
            m.InputSignal = sig1;
            m.N = 4;

            m.Run();

            Assert.IsTrue(expectedOutput == m.OutputAccumlate);
        }
        [TestMethod]
        public void AccumulateSignalTestCases_9()
        {
            // test case 1 ..
            Signal sig1 = new Signal(new List<float>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, false);
            float expectedOutput = 55;

            AccumulateSignal m = new AccumulateSignal();
            m.InputSignal = sig1;
            m.N = 9;

            m.Run();

            Assert.IsTrue(expectedOutput == m.OutputAccumlate);
        }
    }
}
